<?php

use Bittacora\Seo\SeoFacade;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->id();
            $table->json('title');
            $table->json('text')->nullable();
            $table->text('model');
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('featured')->default(0);
            $table->unsignedBigInteger('order_column');
            $table->unsignedBigInteger('parent_id')->nullable();
            SeoFacade::addDatabaseFields($table);
            $table->string('locale')->nullable();
            $table->string('region')->nullable();
            $table->boolean('default');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('category')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('category');
        Schema::enableForeignKeyConstraints();
    }
}
