<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategorizableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorizable', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id');
            $table->morphs('categorizable');
            $table->unsignedBigInteger('order_column')->nullable();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('category')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('categorizable');
        Schema::enableForeignKeyConstraints();
    }
}
