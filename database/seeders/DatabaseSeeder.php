<?php

namespace Bittacora\Category\Database\Seeders;

use Artisan;
use Bittacora\Tabs\Tabs;
use Illuminate\Database\Seeder;
use Bittacora\Category\Database\Seeders\seeds\CategorySeeder;
use Bittacora\Category\Database\Seeders\seeds\CategoryPermissionSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        Tabs::createItem('category.index', 'category.index', 'category.index', 'Categorías','fa fa-list-o');
//        Tabs::createItem('category.create', 'category.create', 'category.create', 'Nueva categoría','fa fa-plus');
//        Artisan::call('rinvex:migrate:categories');
        $this->call([
            CategorySeeder::class,
            CategoryPermissionSeeder::class,
        ]);
    }
}
