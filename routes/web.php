<?php

use Bittacora\Category\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel')->middleware(['web', 'auth','admin-menu'])->name('category.')->group(function(){
    Route::get('category', [CategoryController::class, 'index'])->name('index');
    Route::get('category/create', [CategoryController::class, 'create'])->name('create');
    Route::get('category/{category}', [CategoryController::class, 'show'])->name('show');
    Route::post('category/store', [CategoryController::class, 'store'])->name('store');
    Route::get('category/{model}/edit', [CategoryController::class, 'edit'])->name('edit');
    Route::put('category/{model}', [CategoryController::class, 'update'])->name('update');
    Route::delete('category/{category}', [CategoryController::class, 'destroy'])->name('destroy');
    Route::post('category/reorder', [CategoryController::class, 'reorder'])->name('reorder');
});
