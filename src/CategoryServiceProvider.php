<?php

namespace Bittacora\Category;

use Bittacora\Category\Http\Livewire\CategoryDatatable;
use Bittacora\Category\Http\Livewire\CategoryList;
use Bittacora\Category\Models\CategoryModel;
use Bittacora\Category\Observers\CategoryObserver;
use Bittacora\Category\View\Components\Select;
use Blade;
use Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class CategoryServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package
            ->name('category')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_category_table');
    }

    public function register(){
        $this->app->bind('category', function($app){
            return new Category();
        });
    }

    public function boot(){
        $this->app->register(SeedServiceProvider::class);
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadViewsFrom(__DIR__. '/../resources/views', 'bpanel4-category');
        $this->loadTranslationsFrom(__DIR__ .'/../resources/lang', 'bpanel4-category');
        $this->publishes([
            __DIR__.'/../resources/js' => public_path('vendor/category/js'),
            __DIR__.'/../resources/css' => public_path('vendor/category/css'),
        ], 'category');

        Livewire::component('bpanel4-category::category-datatable', CategoryDatatable::class);
        Livewire::component('bpanel4-category::category-list', CategoryList::class);

        CategoryModel::observe(CategoryObserver::class);

        $this->registerBladeComponents();
    }

    public function registerBladeComponents(): void
    {
       Blade::componentNamespace('Bittacora\\Category\\View\\Components', 'bpanel4-category');
    }

}
