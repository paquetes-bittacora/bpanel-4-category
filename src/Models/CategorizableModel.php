<?php

namespace Bittacora\Category\Models;

use Bittacora\Category\Category;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

/**
 * @property CategoryModel $category
 */
class CategorizableModel extends Model implements Sortable
{
    use SortableTrait;

    public $sortable = [
        'sort_when_creating' => true
    ];

    protected $table = 'categorizable';

    protected $fillable = [
        'category_id'
    ];
    public $incrementing = false;
    public function categorizable()
    {
        return $this->morphTo();
    }

    public function category(): CategoryModel {
        return $this->hasOne(CategoryModel::class, 'id', 'category_id')->firstOrFail();
    }

    public function buildSortQuery()
    {
        return static::query()->where('categorizable_type', $this->categorizable_type)->where('category_id', $this->category_id);
    }
}
