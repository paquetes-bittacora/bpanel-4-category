<?php

declare(strict_types=1);

namespace Bittacora\Category\Models;

use Bittacora\Category\Observers\CategoryObserver;
use Bittacora\Content\Models\ContentModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Spatie\Sluggable\HasTranslatableSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;
use Wildside\Userstamps\Userstamps;

class CategoryModel extends Model implements Sortable
{
    use SortableTrait;
    use HasTranslations;
    use HasTranslatableSlug;
    use Userstamps;

    use HasFactory;

    protected $dispatchesEvents = [
        'deleted' => CategoryObserver::class,
    ];

    protected $fillable = [
        'title',
        'slug',
        'meta_title',
        'meta_keywords',
        'model',
        'meta_description',
        'parent_id',
        'active',
        'featured',
        'default',
    ];

    protected $translatable = [
        'title',
        'slug',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];

    public $sortable = [
        'sort_when_creating' => true,
    ];

    protected $with = ['content', 'children'];

    protected $table = 'category';

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::createWithLocales([$this->getLocale()])
            ->generateSlugsFrom(function ($model, $locale) {
                return (string)($model->title);
            })
            ->saveSlugsTo('slug');
    }

    public function content(): MorphOne
    {
        return $this->morphOne(ContentModel::class, 'model');
    }

    public function children()
    {
        return $this->hasMany(CategoryModel::class, 'parent_id');
    }

    public function buildSortQuery()
    {
        return static::query()->where('model', $this->model)
            ->where('parent_id', $this->parent_id)->orderBy('order_column', 'ASC');
    }

    public function getId(): int
    {
        return $this->id;
    }
}
