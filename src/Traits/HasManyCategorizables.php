<?php

namespace Bittacora\Category\Traits;

use Bittacora\Category\Models\CategorizableModel;
use Bittacora\Category\Models\CategoryModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

trait HasManyCategorizables
{
    /**
     * @return MorphOne<CategorizableModel>
     */
    public function categorizable(): MorphMany
    {
        return $this->morphMany(CategorizableModel::class, 'categorizable');
    }
}
