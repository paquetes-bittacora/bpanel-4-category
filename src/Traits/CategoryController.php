<?php

namespace Bittacora\Category\Traits;

use Bittacora\Category\Http\Requests\StoreCategoryRequest;
use Bittacora\Category\Http\Requests\UpdateCategoryRequest;
use Bittacora\Category\Models\CategoryModel;
use Bittacora\Content\ContentFacade;
use Bittacora\Language\LanguageFacade;
use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

trait CategoryController
{
    private string $permission;
    private string $module;
    private Model $model;

    public function setCategoryControllerProperties(string $modelName): void
    {
        if (!str_contains(url()->current(), 'livewire') and null !== Route::getCurrentRoute()) {
            $this->permission = Route::getCurrentRoute()?->getAction('as');
            $this->module = explode('.', $this->permission)[0];
            $this->model = new $modelName();
        }
    }

    public function category()
    {
        return view('bpanel4-category::index')->with(['model' => $this->model->getMorphClass(), 'module' => $this->module]);
    }

    public function createCategory($language = null)
    {
        $this->authorize($this->permission);
        $contentCategories = CategoryModel::where('model', $this->model->getMorphClass())->pluck('title', 'id');
        $allowedFormats = Multimedia::$allowedFormats;
        $allowedExtensions = Multimedia::$allowedExtensions;

        if (empty($language)) {
            $defaultLanguage = LanguageFacade::getDefault();
            $language = $defaultLanguage->locale;
        }
        return view('bpanel4-category::create', ['language' => $language, 'model' => $this->model->getMorphClass(),
            'contentCategories' => $contentCategories, 'allowedFormats' => $allowedFormats,'allowedExtensions' => $allowedExtensions, 'module' => $this->module, ]);
    }

    public function storeCategory(StoreCategoryRequest $storeCategoryRequest)
    {
        $this->authorize($this->permission);
        $category = new CategoryModel();
        $category->setLocale($storeCategoryRequest->input('locale'));
        $category->fill($storeCategoryRequest->all());
        $category->save();

        ContentFacade::associateWithModel($category);
        if (!empty($_FILES['file']['name'][0])) {
            ContentFacade::associateWithMultimedia($category, $_FILES['file']);
        }

        return redirect()->route($storeCategoryRequest->input('module').'.category')->with(['alert-success' => __('bpanel4-category::category.created')]);
    }

    public function editCategory(CategoryModel $model, $language = null)
    {
        $this->authorize($this->permission);
        if (empty($language)) {
            $language = LanguageFacade::getDefault()->locale;
        }
        $model->setLocale($language);
        $contentCategories = CategoryModel::where('model', $this->model->getMorphClass())->where('id', '!=', $model->id)->pluck('title', 'id');
        $allowedFormats = Multimedia::$allowedFormats;
        $allowedExtensions = Multimedia::$allowedExtensions;

        return view('bpanel4-category::edit', ['language' => $language, 'model' => $this->model->getMorphClass(),
            'contentCategories' => $contentCategories, 'allowedFormats' => $allowedFormats,'allowedExtensions' => $allowedExtensions, 'module' => $this->module, 'category' => $model, ]);
    }

    public function updateCategory(UpdateCategoryRequest $request, CategoryModel $model, $locale)
    {
        $this->authorize($this->permission);
        $oldParentId = $model->parent_id;
        $model->setLocale($request->input('locale'));
        $model->fill($request->all());
        $model->save();

        if ($oldParentId != $request->input('parent_id')) {
            $idsOldLevel = CategoryModel::where('parent_id', $oldParentId)->where('model', '=', $this->model->getMorphClass())->where('id', '!=', $model->id)->orderBy('order_column', 'ASC')->pluck('id')->toArray();
            CategoryModel::setNewOrder($idsOldLevel);

            $idsNewLevel = CategoryModel::where('parent_id', $request->input('parent_id'))->where('model', '=', $this->model->getMorphClass())->where('id', '!=', $model->id)->orderBy('order_column', 'ASC')->pluck('id')->toArray();
            CategoryModel::setNewOrder($idsNewLevel);
            $newMaxOrderColumn = CategoryModel::where('parent_id', $model->parent_id)->where('model', '=', $this->model->getMorphClass())->where('id', '!=', $model->id)->max('order_column');
            CategoryModel::where('id', '=', $model->id)->update(['order_column' => (int)$newMaxOrderColumn+1]);
        }

        ContentFacade::associateWithModel($model);
        if (!empty($_FILES['file']['name'][0])) {
            ContentFacade::associateWithMultimedia($model, $_FILES['file']);
        }

        return redirect()->route($this->module.'.category')->with(['alert-success' => __('bpanel4-category::category.updated')]);
    }

    public function destroyCategory(CategoryModel $category)
    {
        $this->authorize($this->permission);

        if ($category->delete()) {
            Session::put('alert-success', __('bpanel4-category::category.deleted'));
        } else {
            Session::put('alert-warning', __('bpanel4-category::category.not-deleted'));
        }
        return redirect(route($this->module.'.category'));
    }
}
