<?php

namespace Bittacora\Category\Traits;

use Bittacora\Category\Models\CategorizableModel;
use Bittacora\Category\Models\CategoryModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;

trait HasCategorizable
{
    /**
     * @return MorphOne<CategoryModel>
     */

    /**
     * @return MorphOne<CategorizableModel>
     */
    public function categorizable(): MorphOne
    {
        return $this->morphOne(CategorizableModel::class, 'categorizable');
    }
}
