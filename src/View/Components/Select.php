<?php

namespace Bittacora\Category\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\View\Component;

class Select extends Component
{
    /**
     * @param Model|null $model Modelo al que se asignarán las categorías
     * @param Collection|null $categories Categorías disponibles
     * @param bool $multiple Si se permite seleccionar más de una categoría o no
     */
    public function __construct(
        public readonly ?Model $model = null,
        public ?Collection $categories = null,
        public bool $multiple = false
    ) {
    }

    public function render(): View
    {
        return view('bpanel4-category::components.categories-select');
    }
}
