<?php

namespace Bittacora\Category\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Category\Http\Requests\StoreCategoryRequest;
use Bittacora\Category\Http\Requests\UpdateCategoryRequest;
use Bittacora\Category\Models\CategoryModel;
use Bittacora\Language\Models\LanguageModel;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Utilities\Request;

class CategoryController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('category.index');
//        $category1 = app('rinvex.categories.category')->create(['name' => ['en' => 'My New Category'], 'slug' => 'custom-category-slug']);
//        $category2 = app('rinvex.categories.category')->create(['name' => ['en' => 'My New Category1'], 'slug' => 'custom-category-slug1']);
//        $category3 = app('rinvex.categories.category')->create(['name' => ['en' => 'My New Category2'], 'slug' => 'custom-category-slug2']);
//
//        $categoryModel = CategoryModel::firstOrCreate([
//            'title' => 'London to Paris'
//        ]);
//
////        $categoryModel->attachCategories(1,2,3);
////        $categoryModel->save();
//
//        dd($categoryModel->categories());
        return view('bpanel4-category::index')->with(['categories' => CategoryModel::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('category.create');
        return view('bpanel4-category::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCategoryRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(StoreCategoryRequest $request): RedirectResponse
    {
        $this->authorize('category.create');
        $category = new CategoryModel($request->all());
        $category->save();

        Session::put('alert-success', 'Categoría creada correctamente');
        return redirect(route('category.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param CategoryModel $category
     * @return View
     * @throws AuthorizationException
     */
    public function show(CategoryModel $category): View
    {
        $this->authorize('category.show');
        return view('bpanel4-category::show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param CategoryModel $category
     * @return View
     * @throws AuthorizationException
     */
    public function edit(CategoryModel $category): View
    {
        $this->authorize('category.edit');
        return view('bpanel4-category::edit')->with([
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCategoryRequest $request
     * @param CategoryModel $category
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(UpdateCategoryRequest $request, CategoryModel $category): RedirectResponse
    {
        $this->authorize('category.edit');
        $category->update($request->all());
        Session::put('alert-success', 'Categoría editada correctamente');
        return redirect(route('category.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CategoryModel $category
     * @throws AuthorizationException
     */
    public function destroy(CategoryModel $category)
    {
        $this->authorize('category.destroy');
        if ($category->delete()){
            Session::put('alert-success', 'Categoría borrada correctamente');
        }else{
            Session::put('alert-warning', 'No se puede borrar esta categoría');
        }
        return redirect(route('category.index'));
    }

    /**
     * @param Request $request
     */
    public function reorder(Request $request)
    {
        $collect=collect($request->json()->all());
        CategoryModel::setNewOrder($collect->pluck('id'),$collect->first()['position']);
    }
}
