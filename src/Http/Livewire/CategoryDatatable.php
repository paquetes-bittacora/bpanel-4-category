<?php

namespace Bittacora\Category\Http\Livewire;

use Bittacora\Category\Models\CategoryModel;
use Bittacora\Content\ContentFacade;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rinvex\Categories\Models\Category;

class CategoryDatatable extends DataTableComponent
{

    public $parent_id = null;
    public $module = null;
    public $model = null;
    public bool $reordering = true;
    public string $reorderingMethod = 'reorder';
    public array $perPageAccepted = [10,25,50,100];
    public string $emptyMessage = "El registro de categorías está vacío";
    public bool $showSearch = true;

    public function columns(): array
    {
        return [
            Column::make('Nombre', 'title')->addClass('w-30'),
            Column::make('Idiomas')->addClass('w-20 text-center'),
            Column::make('Categorías hijas')->addClass('w-10 text-center'),
            Column::make('Orden', 'order_column')->addClass('w-10 text-center'),
            Column::make('Acciones')->addClass('w-10')
        ];
    }

    public function query(): Builder
    {
        return CategoryModel::query()->where('model', $this->model)->where('parent_id', $this->parent_id)->orderBy('order_column', 'ASC')->
        when($this->getFilter('search'), fn ($query, $term) => $query->where([
            ['title->es', 'like', '%'.strtoupper($term).'%'],
            ['model', '=', $this->model]
        ])->orWhere([
            ['title->es', 'like', '%'.strtolower($term).'%'],
            ['model', '=', $this->model]
        ])->orWhere([
            ['title->es', 'like', '%'.ucfirst($term).'%'],
            ['model', '=', $this->model]
        ]));
    }

    public function rowView(): string
    {
        return 'bpanel4-category::livewire.category-datatable';
    }

    public function reorder($list){
        foreach($list as $item){
            CategoryModel::where('id', $item['value'])->where('parent_id', $this->parent_id)->where('model', $this->model)->update(['order_column' => $item['order']]);
        }
    }

    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar'
        ];
    }

    public function bulkDelete(){
        if(count($this->selectedKeys())){
            foreach($this->selectedKeys() as $key){
                $category = CategoryModel::where('id', $key)->with('children') -> first();
                $category->delete();
                ContentFacade::deleteModelContent($category);
            }
            $this->resetAll();
        }
    }
}
