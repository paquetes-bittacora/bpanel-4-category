<?php

namespace Bittacora\Category\Http\Livewire;

use Bittacora\Category\Models\CategoryModel;
use Livewire\Component;
use function view;

class CategoryList extends Component
{
    public $model;
    public $categories;
    public $parent_id = null;


    protected $listeners = ['updateCategoryOrder' => '$refresh'];

    public function mount(){
        $this->categories = CategoryModel::where('model', $this->model)->where('parent_id', $this->parent_id)->orderBy('order_column', 'ASC')->get();
    }

    public function render()
    {
        return view('bpanel4-category::livewire.category-list');
    }

    public function updateCategoryOrder($items){
        foreach($items as $item){
            CategoryModel::where('id', $item['value'])->where('model', $this->model)->where('parent_id', $this->parent_id)->update(['order_column' => $item['order']]);
        }
        $this->emitSelf('$refresh');
    }
}
