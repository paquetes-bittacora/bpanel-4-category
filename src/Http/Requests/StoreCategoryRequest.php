<?php

namespace Bittacora\Category\Http\Requests;

use Bittacora\Seo\SeoFacade;
use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest
{
    protected function prepareForValidation(): void
    {
        $this->request->add(['active' => $this->has('active')]);
        $this->request->add(['featured' => $this->has('featured')]);
        $this->request->add(['default' => $this->has('default')]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, string>
     */
    public function rules(): array
    {
        $rules = [
            'title' => 'required',
            'active' => 'nullable|boolean',
            'featured' => 'nullable|boolean',
            'default' => 'nullable|boolean',
        ];

        return SeoFacade::addRequestValidationRules($rules, 'category');
    }
}
