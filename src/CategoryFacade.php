<?php

namespace Bittacora\Category;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Category\Category
 */
class CategoryFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'category';
    }
}
