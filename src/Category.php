<?php

namespace Bittacora\Category;

use Bittacora\Category\Models\CategoryModel;
use Bittacora\ContentMultimedia\ContentMultimediaFacade;
use Bittacora\Tabs\Tabs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Category
{
    public function getModulePermissions(string $modulePrefix)
    {
        return Permission::all()->filter(function ($value, $key) use ($modulePrefix) {
            return 0 === strpos($value->name, $modulePrefix);
        });
    }

    public function getNamespace()
    {
    }

    public function createTabs(string $module, string $indexTitle)
    {
        Tabs::createItem($module.'.index', $module.'.category', $module.'.category', 'Categorías', 'fa fa-sitemap');
        Tabs::createItem($module.'.create', $module.'.category', $module.'.category', 'Categorías', 'fa fa-sitemap');
        Tabs::createItem($module.'.edit', $module.'.category', $module.'.category', 'Categorías', 'fa fa-sitemap');

        Tabs::createItem($module.'.category', $module.'.category', $module.'.category', 'Categorías', 'fa fa-sitemap');
        Tabs::createItem($module.'.category', $module.'.createCategory', $module.'.createCategory', 'Nueva categoría', 'fa fa-plus');
        Tabs::createItem($module.'.category', $module.'.index', $module.'.index', $indexTitle, 'fa fa-list');

        Tabs::createItem($module.'.createCategory', $module.'.category', $module.'.category', 'Categorías', 'fa fa-sitemap');
        Tabs::createItem($module.'.createCategory', $module.'.createCategory', $module.'.createCategory', 'Nueva categoría', 'fa fa-plus');
        Tabs::createItem($module.'.createCategory', $module.'.index', $module.'.index', $indexTitle, 'fa fa-list');

        Tabs::createItem($module.'.editCategory', $module.'.category', $module.'.category', 'Categorías', 'fa fa-sitemap');
        Tabs::createItem($module.'.editCategory', $module.'.createCategory', $module.'.createCategory', 'Nueva categoría', 'fa fa-plus');
        Tabs::createItem($module.'.editCategory', $module.'.index', $module.'.index', $indexTitle, 'fa fa-list');
    }

    public function addRoutes(string $module, string $model, string $controller)
    {
        Route::get($module.'/category', [$controller, 'category'])->name('category');
        Route::get($module.'/category/create/language/{locale?}/', [$controller,'createCategory'])->name('createCategory');
        Route::post($module.'/category/store/', [$controller, 'storeCategory'])->name('storeCategory');
        Route::get($module.'/category/{model}/edit/language/{locale?}', [$controller, 'editCategory'])->name('editCategory');
        Route::put($module.'/category/{model}/update/language/{locale?}', [$controller, 'updateCategory'])->name('updateCategory');
        Route::delete($module.'/category/{category}', [$controller, 'destroyCategory'])->name('destroyCategory');
    }

    public function addPermission(string $module)
    {
        $permissions = ['category', 'createCategory', 'editCategory', 'destroyCategory', 'storeCategory', 'updateCategory'];

        $adminRole = Role::findOrCreate('admin');
        foreach ($permissions as $permission) {
            $permission = Permission::create(['name' => $module.'.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function addCategoriesToModel(Model $model, string|array $categories): void
    {
        $categories = $this->formatCategoriesArray($categories);
        if (!$model->categorizable()->exists()) {
            foreach ($categories as $categoryId) {
                $category = CategoryModel::where('id', $categoryId)->first();
                $model->categorizable()->make(['category_id' => $category->id])->save();
            }
        } else {
            $this->deleteModelCategorizable($model);
            $this->addCategoriesToModel($model, $categories);
        }
    }

    public function associateToModel(Model $model, null|array|string $category = null): void
    {
        if (is_null($category)) {
            $this->deleteModelCategorizable($model);
            return;
        }
        $this->addCategoriesToModel($model, $category);
    }

    private function formatCategoriesArray(string|array $categories): array
    {
        if (is_string($categories)) {
            $categories = [$categories];
        }

        return array_map(static fn ($category) => (int)$category, $categories);
    }

    public function deleteModelCategorizable(Model $model)
    {
        if ($model->categorizable()->exists()) {
            $model->categorizable()->delete();
        }
    }

    public function getAllAsArray($model)
    {
        $output = [];
        $categories = CategoryModel::where('model', $model)->whereNull('parent_id')
            ->orderBy('order_column', 'ASC')->get();

        foreach ($categories as $category) {
            $output[$category->id] = $category->title;

            $this->addChildCategoriesToArray($category, $category->title, $output);
        }

        return new Collection($output);
    }

    public function getCategoriesFromModule($model)
    {
        return CategoryModel::where('model', $model)->orderBy('order_column', 'ASC')->get();
    }

    public function getFeaturedCategories($model)
    {
        $categories = CategoryModel::where('model', $model)->where('active', 1)->where('featured', 1)->orderBy('order_column', 'ASC')->get();

        foreach ($categories as $key => $value) {
            $categories[$key]->images = ContentMultimediaFacade::retrieveContentImages('category', $value->content->id);
        }

        return $categories;
    }

    public function getBySlug($slug)
    {
        $category = CategoryModel::where('slug->'.app()->getLocale(), $slug)->first();
        if (is_null($category)) {
            abort(404);
        }
        $category->images = ContentMultimediaFacade::retrieveContentImages('category', $category->content->id);
        return $category;
    }

    private function addChildCategoriesToArray(mixed $category, string $prefix, array &$output): void
    {
        if (!str_ends_with($prefix, ' > ')) {
            $prefix .= ' > ';
        }

        if ($category->has('children')) {
            foreach ($category->children()->get() as $childCategory) {
                $categoryName = $prefix . $childCategory->title;
                $output[$childCategory->id] = $categoryName;
                $this->addChildCategoriesToArray($childCategory, $categoryName, $output);
            }
        }
    }
}
