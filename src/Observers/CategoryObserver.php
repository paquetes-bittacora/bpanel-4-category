<?php

namespace Bittacora\Category\Observers;

use Bittacora\Category\Models\CategoryModel;
use Bittacora\Content\ContentFacade;

class CategoryObserver
{
    public function deleted(CategoryModel $category){
        $parentId = $category->parent_id;
        if(!$category->children->isEmpty()){
            $children = $category->children->pluck('id')->toArray();
            $ids = CategoryModel::where('parent_id', '=', $parentId)->where('model', '=', $category->model)->whereNotIn('id', $children) -> ordered()->pluck('id')->toArray();
            CategoryModel::setNewOrder(array_merge($ids, $children));
        }else{
            $ids = CategoryModel::where('parent_id', '=', $parentId)->where('model', '=', $category->model) -> ordered()->pluck('id')->toArray();
            CategoryModel::setNewOrder($ids);
        }
        // Reordeno después de borrar


        // Borro el content
        ContentFacade::deleteModelContent($category);
    }
}
