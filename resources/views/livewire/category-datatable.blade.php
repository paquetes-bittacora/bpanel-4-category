<td>
    {{$row->title}}
</td>
<td>
    @livewire('language::datatable-languages', ['model' => $row, 'createRouteName' => $module.'.createCategory', 'editRouteName' => $module.'.editCategory'], key('languages-content-'.$row->id))
</td>
<td>
    <div class="d-flex justify-content-center align-items-center">
        <span class="align-items-center badge bgc-purple-d1 pos-rel text-white radius-4 px-3">
            <span class="bgc-primary-tp4 opacity-5 position-tl h-100 w-100 radius-4"></span>
            <span class="pos-rel">
                {{$row->children->count()}}
            </span>
        </span>
    </div>
</td>
<td>
    <div class="d-flex justify-content-center align-items-center">
        <span class="align-items-center badge bgc-purple-d1 pos-rel text-white radius-4 px-3">
            <span class="bgc-primary-tp4 opacity-5 position-tl h-100 w-100 radius-4"></span>
            <span class="pos-rel">
                {{$row->order_column}}
            </span>
        </span>
    </div>
</td>
<td>
    <div class="action-buttons">
        <a class="text-success mx-2" href="{{route($module.'.editCategory', $row)}}">
            <i class="fa fa-pencil"></i>
        </a>

        <form method="POST" class="d-inline mx-2"
              action="{{route($module.'.destroyCategory', $row)}}">
            {!!method_field('DELETE')!!}
            @csrf
            <button id="delete_button{{$row->id}}" type="submit" class="btn btn-primary-outline text-danger"
                    onclick="return confirm('¿Está seguro de querer borrar la categoría?')">
                <i class="fa fa-trash"></i>
            </button>
        </form>
    </div>
</td>
