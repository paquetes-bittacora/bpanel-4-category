<div>
    <ul wire:sortable="updateCategoryOrder">
        @foreach ($categories as $category)
            <li wire:sortable.item="{{ $category->id }}" wire:key="category-{{ $category->id }}">
                <h4 wire:sortable.handle>{{ $category->name }}</h4>
{{--                <button wire:click="removeTask({{ $category->id }})">Remove</button>--}}
            </li>
        @endforeach
    </ul>
</div>
