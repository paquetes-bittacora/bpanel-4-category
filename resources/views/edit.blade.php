@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Editar categoría')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120">
                <span class="text-90">{{ __('bpanel4-category::category.edit') }}</span>
            </h4>
            @include('language::partials.form-languages', ['model' => $category, 'edit_route_name' => $module.'.editCategory',  'currentLanguage' => $language])
        </div>
        <div class="card-body">
            <form class="mt-lg-3" autocomplete="off" method="post" action="{{route($module.'.updateCategory', ['model' => $category, 'locale' => $language])}}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                @livewire('form::input-text', ['name' => 'title', 'labelText' => __('bpanel4-category::category.name'), 'required'=>true, 'value' => $category->title])
                @livewire('utils::tinymce-editor', ['name' => 'text', 'labelText' => __('bpanel4-category::category.description'), 'value' => $category->text])
                @livewire('form::select', ['name' => 'parent_id', 'allValues' => $contentCategories, 'labelText' => __('bpanel4-category::category.parent-category'), 'emptyValue' => true, 'emptyValueText' => 'Sin categoría superior', 'selectedValues' => [$category->parent_id]])
                @livewire('form::input-checkbox', ['name' => 'active', 'checked' => $category->active == 1, 'labelText' => __('bpanel4-category::category.active'), 'bpanelForm' => true])
                @livewire('form::input-checkbox', ['name' => 'featured', 'checked' => $category->featured == 1, 'labelText' => __('bpanel4-category::category.featured'), 'bpanelForm' => true])
                @if(!$category->children->isEmpty())
                    <div class="widget-box widget-color-blue ui-sortable-handle mt-3" id="widget-box-7">
                        <div class="widget-header widget-header-small">
                            <h6 class="widget-title smaller">Categorías hijas</h6>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                @livewire('bpanel4-category::category-datatable', ['parent_id' => $category->id, 'model' => $model, 'module' => $module, 'showSearch' => false])
                            </div>
                        </div>
                    </div>
                @endif
                @livewire('content-multimedia::upload-content-multimedia-widget', ['model' => $category, 'allowedFormats' => $allowedFormats, 'allowedExtensions' => $allowedExtensions])
                @livewire('content-multimedia-images::content-multimedia-images-widget', ['contentId' => $category->content->id, 'module' => 'category', 'permission' => $module.'updateCategory'])
                @livewire('seo::seo-fields', ['model' => $category])

                <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brvgc-secondary-l2 py-35 d-flex justify-content-center">
                    @livewire('form::save-button',['theme'=>'save'])
                    @livewire('form::save-button',['theme'=>'reset'])
                </div>
                @livewire('form::input-hidden', ['name' => 'locale', 'value'=> $language])
                @livewire('form::input-hidden', ['name' => 'id', 'value'=> $category->id])
                @livewire('form::input-hidden', ['name' => 'model', 'value' => $model])
            </form>
        </div>

    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/gh/livewire/sortable@v0.x.x/dist/livewire-sortable.js"></script>
@endpush
