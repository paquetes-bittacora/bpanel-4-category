@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Categorías')

@section('content')
    <p class="alert alert-success">
        {!!  __('blog::blog.create_default_language') !!}
    </p>
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120">
                <span class="text-90">{{ __('bpanel4-category::category.add') }}</span>
            </h4>
            <img src="{{asset('img/flags/'.$language.'.png')}}" alt="{{ $language }}">
        </div>
        <div class="card-body">
            <form class="mt-lg-3" autocomplete="off" method="post" action="{{route($module.'.storeCategory')}}" enctype="multipart/form-data">
                @csrf
                    @livewire('form::input-text', ['name' => 'title', 'labelText' => __('bpanel4-category::category.name'), 'required'=>true])
                    @livewire('utils::tinymce-editor', ['name' => 'text', 'labelText' => __('bpanel4-category::category.description')])
                    @livewire('form::input-checkbox', ['name' => 'active', 'checked' => true, 'labelText' => __('bpanel4-category::category.active'), 'bpanelForm' => true])
                    @livewire('form::input-checkbox', ['name' => 'featured', 'checked' => false, 'labelText' => __('bpanel4-category::category.featured'), 'bpanelForm' => true])

                    @livewire('form::select', ['name' => 'parent_id', 'allValues' => $contentCategories, 'labelText' => __('bpanel4-category::category.parent-category'), 'placeholder' => 'Sin categoría superior'])
                    @livewire('content-multimedia::upload-content-multimedia-widget', ['model' => null, 'allowedFormats' => $allowedFormats, 'allowedExtensions' => $allowedExtensions])
                    @livewire('seo::seo-fields')
                <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                    @livewire('form::save-button',['theme'=>'save'])
                    @livewire('form::save-button',['theme'=>'reset'])
                </div>
                @livewire('form::input-hidden', ['name' => 'model', 'value' => $model])
                @livewire('form::input-hidden', ['name' => 'locale', 'value'=> $language])
                @livewire('form::input-hidden', ['name' => 'module', 'value'=> $module])
            </form>
        </div>

    </div>
@endsection
