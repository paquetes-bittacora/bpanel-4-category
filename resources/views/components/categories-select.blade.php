<div>
    @if($model?->categorizable()->exists())
        @livewire('form::select', [
        'name' => $multiple ? 'category[]' : 'category',
        'allValues' => $categories,
        'labelText' => $multiple ? __('bpanel4-category::model.categories') : __('bpanel4-category::model.category'),
        'selectedValues' => $model->categorizable()->pluck('category_id')->toArray(),
        'emptyValue' => true,
        'emptyValueText' => 'Sin categoría',
        'multiple' => $multiple
        ])
    @else
        @livewire('form::select', [
        'name' => $multiple ? 'category[]' : 'category',
        'allValues' => $categories,
        'labelText' => $multiple ? __('bpanel4-category::model.categories') : __('bpanel4-category::model.category'),
        'emptyValue' => true,
        'emptyValueText' => 'Sin categoría',
        'multiple' => $multiple
        ])
    @endif
    @if ($multiple)
        @pushonce('scripts')
            <script>
              document.addEventListener('DOMContentLoaded', function () {
                $('.select2').select2();
              });
            </script>
        @endpushonce
    @endif
</div>
