<?php

declare(strict_types=1);

return [
    'category' => 'Categoría',
    'categories' => 'Categorías'
];