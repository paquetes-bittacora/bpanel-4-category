<?php

return[
    'index' => 'Listar categorías',
    'create' => 'Crear categoría',
    'show' => 'Mostrar categoría',
    'edit' => 'Editar categoría',
    'delete' => 'Borrar categoría',
    'add' => 'Crear nueva categoría',
    'category_name' => 'Nombre',
    'category_list' => 'Listado de categorías',
    'category_editing' => 'Editando categoría',
    'active' => 'Activada',
    'name' => 'Nombre',
    'slug' => 'Ruta',
    'destroy' => 'Eliminar categoría',
    'created' => 'Categoría creada correctamente',
    'updated' => 'Categoría actualizada correctamente',
    'deleted' => 'La categoría ha sido eliminada',
    'description' => 'Descripción',
    'parent-category' => 'Categoría superior',
    'featured' => 'Destacada',
];
