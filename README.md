# Categorías

Módulo para añadir categorías a cualquier módulo de bPanel 4.

## Instalación

Para instalar las categorías en un módulo, hay que seguir varios pasos.

### Templates

En los templates de create y edit, hay que añadir el select de categorías:

```  
<x-bpanel4-category::select :model="$model" :categories="$categories" :multiple="true"/>
```
Los parámetros son:
- **model:** modelo al que se quieren asignar las categorías
- **categories:** `Collection` de categorías disponibles, por ejemplo, categorías de productos si el modelo es un producto, categorías del blog si es una entrada, etc
- **multiple**: si se puede seleccionar más de una categoría

### Traducciones

En el archivo `breadcrumbs.php` de las traducciones del módulo, añadir:

```php  
'editCategory' => 'Editar categoría',  
'createCategory' => 'Crear categoría',  
'category' => 'Categorías',  
```  
En el archivo principal de traducciones (por ejemplo, `product.php` si estamos en el módulo `product`, añadir:

```php  
'active' => 'Activado',  
'featured' => 'Destacado',  
```  

### Comando de instalación

Si el módulo tiene un comando de instalación, habrá que añadir lo siguiente en el método `handle`;

```php  
  
AdminMenuFacade::createAction('module', 'Categorías', 'category', 'fa fa-sitemap');  
CategoryFacade::addPermission('module');  
CategoryFacade::createTabs('modulo', 'Listado de XXXXXX'); // Productos, entradas, etc  
  
```  

### Modelo

En el modelo al que se quieren añadir categorías, añadir:

```php  
use HasCategorizable;  
```  


### Controlador

Añadir:

```php  
use CategoryController;  
```  

En el constructor, llamar a:

```php  
$this->setCategoryControllerProperties(Model::class);  
```  

Habrá que sustituir `Model` por la clase correspondiente al modelo del módulo al que se quieren añadir categorías.

#### Métodos del controlador:
Hay que añadir lo siguiente a los distintos métodos del controlador, sustituyendo `Model` por el modelo del módulo:

**Create y edit**  
Pasar la variable `categories` a la vista:

```php  
->with([  
...  
'categories' => CategoryFacade::getAllAsArray(Model::class),  
...  
```  

**Store y  update**
```php  
CategoryFacade::associateToModel($model, $category);
```  

Parámetros:
- **model:** modelo al que se quieren asociar las categorías
- **category:** el resultado de `$request->input('category')`, donde `category` es el `name` del campo `<x-bpanel4-category::select/>` ('category' por defecto)

*Nota: Model debe haber sido guardado (debe tener id)*


**Destroy**  
*Nota: no he comprobado este método al hacer la documentación, que alguien lo actualice si hay que hacer algún cambio.*
```php  
if (is_null($request->input('category'))) {    
    CategoryFacade::deleteModelCategorizable($model);
} else {    
    CategoryFacade::associateWithCategory($model, $request->input('category'));
}  
```  
### Rutas

```php  
  
CategoryFacade::addRoutes('module', Model::class, ModuleController::class);  
  
```  

Habrá que sustituir `'module'` por el nombre del módulo, `Model` por la clase del modelo y `ModuleController` por el controlador al que hemos añadido el trait `CategoryController`.

## Ejemplos

### Filtrar por varias categorías

Si se quieren seleccionar todos los productos que están en alguna de las categorías seleccionadas, puede usarse lo siguiente: 

```php
$model->whereHas('categorizable', function ($model) use ($categories) {
    $model->whereIn('category_id', $categories);
});
```

`$categories` es un array con las categorías por las que se quiere filtrar.
